import com.rockymadden.stringmetric.phonetic.{MetaphoneAlgorithm, RefinedNysiisAlgorithm, RefinedSoundexAlgorithm}
import com.rockymadden.stringmetric.similarity.{JaroMetric, LevenshteinMetric}

object Test {
  def Phonetics(s: String): String = RefinedNysiisAlgorithm.compute(s).get

  def NumsError(s1: String, s2: String): Int = LevenshteinMetric.withMemoization.compare(s1, s2).get

  "Jana".toDouble
  "Yana".toDouble

  val n1 = ("Kseniya", "Ivanova")
  val n2 = ("Kseniya", "Ivvanova")
  val n3 = ("Petrova", "Yana")
  val n4 = ("Jana", "Petrova")
  val n5 = ("Pavel", "Kondratyuk")
  val n6 = ("Pavel", "Kondratuyk")
  val n7 = ("Roman", "Slipencuk")
  val n8 = ("Roma", "Slipenchuk")
  JaroMetric.compare(n3._1, n4._1)
  JaroMetric.compare(n1._2, n2._2)
  val sound1 = RefinedSoundexAlgorithm.compute(n3._1)
  val sound2 = RefinedSoundexAlgorithm.compute(n4._1)
  MetaphoneAlgorithm.compute(n3._1)
  MetaphoneAlgorithm.compute(n4._1)
  "--------------------------------------------------"
  MetaphoneAlgorithm.compute(n1._2)
  MetaphoneAlgorithm.compute(n2._2)
  "--------------------------------------------------"
  RefinedNysiisAlgorithm.compute(n3._1)
  RefinedNysiisAlgorithm.compute(n4._1)
  "--------------------------------------------------"
  RefinedNysiisAlgorithm.compute(n1._2)
  RefinedNysiisAlgorithm.compute(n2._2)

  def comparePeopleQuick(p1: (String, String), p2: (String, String)): Double = {
    if (NumsError(p1._1, p2._1) > NumsError(p1._1, p2._2)) comparePeopleQuick(p1, p2.swap) else {
      (NumsError(p1._1, p2._1) + NumsError(p1._2, p2._2) +
        NumsError(Phonetics(p1._1), Phonetics(p2._1)) + NumsError(Phonetics(p1._2), Phonetics(p2._2)))
    }
  }


  def comparePeople(p1: (String, String), p3: (String, String)): Boolean = {
    var p2 = p3
    if (NumsError(p1._1, p3._1) > NumsError(p1._2, p3._1)) p2 = p3.swap
      val sim1 = NumsError(p1._1, p2._1)
      val sim2 = NumsError(p1._2, p2._2)
      if (sim1 + sim2 == 0) true
      else if (sim1 + sim2 > 4) false
      else {
        val phon_err1 = NumsError(Phonetics(p1._1), Phonetics(p2._1))
        val phon_err2 = NumsError(Phonetics(p1._2), Phonetics(p2._2))
        if (phon_err1 < 3 && phon_err2 < 3 && (phon_err1 + phon_err1) < 4 )
          true
        else
          false
    }
  }

  comparePeople(n1, n2)
  comparePeople(n3, n4)
  comparePeople(n5, n6)
  comparePeople(n7, n8)

  comparePeopleQuick(n1, n2)
  comparePeopleQuick(n3, n4)
  comparePeopleQuick(n5, n6)
  comparePeopleQuick(n7, n8)
}
