import com.rockymadden.stringmetric.phonetic.RefinedNysiisAlgorithm
import com.rockymadden.stringmetric.similarity.LevenshteinMetric

import scala.collection.mutable.ArrayBuffer

object MetricCalculator {
  def Phonetics(s: String): String = RefinedNysiisAlgorithm.withMemoization.compute(s).get

  def NumsError(s1: String, s2: String): Int = LevenshteinMetric.withMemoization.compare(s1, s2).get

  def mapPersonBrthday(attr: Array[String]): (String, (String, String)) = {
    val personNames = (attr(0), attr(1))
    //Birthdate as Key
    (attr(2), personNames)
  }

  def clusterList(list: Iterable[(String, String)]): Iterable[(String, String, String)] = {
    val result = new ArrayBuffer[(String, ArrayBuffer[(String, String)])]
    list.foreach(x => {
      val el = result.indexWhere(y => comparePeople(y._2(0), x))
      if (el == -1)
        result.append((x._1 + x._2, ArrayBuffer(x)))
      else
        result(el)._2.append(x)
    })
    result.flatMap(x => x._2.map(y => (x._1, y._1, y._2)))
  }

  def comparePeople(p1: (String, String), p3: (String, String)): Boolean = {
    var p2 = p3
    if (NumsError(p1._1, p3._1) > NumsError(p1._2, p3._1)) p2 = p3.swap
    val sim1 = NumsError(p1._1, p2._1)
    val sim2 = NumsError(p1._2, p2._2)
    if (sim1 + sim2 == 0) true
    else if (sim1 + sim2 > 4) false
    else {
      val phon_err1 = NumsError(Phonetics(p1._1), Phonetics(p2._1))
      val phon_err2 = NumsError(Phonetics(p1._2), Phonetics(p2._2))
      if (phon_err1 < 3 && phon_err2 < 3 && (phon_err1 + phon_err1) < 4)
        true
      else
        false
    }
  }

  def comparePeopleQuick(p1: (String, String), p3: (String, String)): Double = {
    var p2 = p3
    if (NumsError(p1._1, p3._1) > NumsError(p1._2, p3._1)) p2 = p3.swap
    (NumsError(p1._1, p2._1) + NumsError(p1._2, p2._2) +
      NumsError(Phonetics(p1._1), Phonetics(p2._1)) + NumsError(Phonetics(p1._2), Phonetics(p2._2)))
  }


}
