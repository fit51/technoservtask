//import org.apache.spark.sql.Row
//import org.apache.spark.sql.types._
import org.apache.spark.sql.SparkSession
import scala.util.hashing.MurmurHash3

/**
  * Created by pavel on 24.10.16.
  */
object Main {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.
      master("local")
      .appName("Simple Application")
      .getOrCreate()
    val input = spark.sparkContext.textFile("/home/pavel/IdeaProjects/PeopleSegmentation/Source.txt").cache()

    /*val schemaString = "name1 name2 birthday"
    val fields = schemaString.split(" ")
      .map(fieldName => StructField(fieldName, StringType, nullable = true))
    val schema = StructType(fields)

    val rowRDD = input.map(_.split("\\|"))
      .map(attributes => Row(attributes(0), attributes(1)))
    val peopleDF = spark.createDataFrame(rowRDD, schema).*/

    val people_rdd = input.map(_.split("\\|"))
      .map(MetricCalculator.mapPersonBrthday)
      .groupByKey().flatMap(x => MetricCalculator.clusterList(x._2)
      .map(y => (MurmurHash3.stringHash(y._1 + x._1), y._2, y._3, x._1)))
    people_rdd.foreach(x => println(x._1 + " | " + x._2 + " | " + x._3))

  }

}
